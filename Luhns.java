import java.io.IOException;

public class Luhns {

	public static boolean check_card_number(String number) {
		
		
		String[] numberEv = new String[number.length()];
		String[] numberOd = new String[number.length()];
		
		for(int i = number.length()-1; i >-1;i--) {
			if((i+1)%2==0) {numberEv[i]=number.charAt(i)+"";}
			else {numberOd[i]=number.charAt(i)+"";}
			
		}
		
		
		
		
		for(int i = 0; i <number.length();i++ ) {
		
			if(numberEv[i]==null) {numberEv[i]="0";}
			else {
				
				
				int s =Integer.parseInt(numberEv[i])*2;
				if(s>=10) {
					
					String numO=(s+"").charAt(0)+"";
					String numT =(s+"").charAt(1)+"";
					numberEv[i]=(Integer.parseInt(numO)+Integer.parseInt(numT))+"";
							
				}
				else {numberEv[i] = s+"";}	
				
			}
		}
		
		int total = 0;
		
		for(String s :numberEv ) {total =total+Integer.parseInt(s);}
		for(String s :numberOd ) {if(s!=null) {total =total+Integer.parseInt(s);}}
		
	
		//just shows Odd and Even digits you can delete this
		/*
		System.out.println("Even");
		for(String s :numberEv ) {if(!s.equals("0")) {System.out.println(s);}}
		System.out.println("Odd");
		for(String s :numberOd ) {if(s!=null) {System.out.println(s);}}
		System.out.println(total);
		*/
		
		
		if(total%10==0) {return true;}
		else {return false;}
		
	}
	
	
	
	
	 public static void main(String[]args ) throws IOException {
		 //You have to put the card number in as a string int is too big
		 System.out.println( check_card_number("378808902784172"));
		//375848434369517
		 //345225278867541
		//79927398713
		//49927398716
		 //349603945903459
		 //378808902784172
		
		 
	 }
}