import java.util.ArrayList;
import java.util.Scanner;

public class StoreFront {

	public static void main(String[] args) {
		int userInput;
		boolean loop = true;
		String prodSelect = null;
		ArrayList<Items> prodList = new ArrayList<>(); 
		
		// Create a Scanner object to read input.
	    Scanner keyboard = new Scanner(System.in);
		
	    // Display Welcome Message
		System.out.println("===========================\n" +
		                   " Welcome to the Golf Store\n" +
				           "===========================\n");
		
		// Select Product Category		
		while (loop) {
			System.out.println("Select Product Category:\n"
					+ "[1] Golf Clubs, [2] Golf Balls, [3] Golf Shirts");
			
			userInput = keyboard.nextInt();
			if (userInput == 1 || userInput == 2 || userInput == 3) {
				prodSelect = ItemSelection.Product(userInput);
				loop = false;
			}
			else {
				System.out.println("Invalid Product Selection\n");
				loop = true;
			}
		}
		
		// Load Product List
		prodList = ItemSelection.ProductList(prodSelect);	
		
		//Display Product List
		for (Items item: prodList) {
            System.out.println(item);
        }
		
		System.out.println("Which Item would you like to add to the shopping cart?");
		// Select Product	
		System.out.print("Enter Item Number to Add to Cart: ");
		userInput = keyboard.nextInt();
		ArrayList<Items> cart = new ArrayList<>();
		//cart = ShoppingCart


	}
	
	public static String Product(int userInput) {
		String product = "";
			switch (userInput) {
			case 1:
				product = "Golf Clubs";
				break;
			case 2:
				product = "Golf Balls";
				break;
			case 3:
				product = "Golf Shirts";
				break;
			default:
				System.out.println("Invalid Team Selection\n");
				System.exit(0);
				break;
			}	
		
		System.out.println("Loading " + product + " items\n");
		return product;
	}

}