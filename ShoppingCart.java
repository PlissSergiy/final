import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

/**
 * 
 */

/**
 * @author ryankropp
 *
 */
class ShoppingCart {

 
	
	
	
	
	
	
	

  ArrayList<Items> cart;
  int itemsInCart;
  double cartTotal;

 

  public ShoppingCart() {
    this.cart = new ArrayList<Items>();
    this.itemsInCart = 0;
    this.cartTotal = 0.0d;
  }

 

  public void addItem(Items item) {

    this.cart.add(item);

    itemsInCart++;

  }

 

  public void removeItem(int index) {

    cart.remove(index);

    itemsInCart--;

  }

 

  public void clearCart() {

    cart.clearAll();

    itemsInCart = 0;

  }

 

  public double getCartTotal() {

    for (int i = 0; i <= cart.size(); i++) {

                cartTotal = Items.getPrice(i) + cartTotal;

      }

    return cartTotal;

  }

 

  public void showCart() {

    System.out.println("Your Shopping Cart\n" +

                    "=========================\n");

                System.out.println("Items in your Cart:\n");

    for (int i = 0; i <= cart.size(); i++) {

                System.out.println(cart.get(i));

      }

                System.out.println("Total: " + this.getCartTotal());                   

  }

}
