
public class Shirt extends TangibleItem{
	
	String color;
	String size;
	
	public Shirt(String sn, String name, String make, double price, String size, String color)
	{
		super(sn,name, make,price);
		this.size = size;
		this.color = color;
		
	}

	@Override
	public String toString() {
		return "StockNumber=" + stockNumber + ", size=" + size + ", shirt color=" + color + ", name=" + name
				+ ", make=" + make + ", price=" + price + " ";
	}
	
	
	
	
	

}
